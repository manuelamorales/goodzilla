﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
    public PlayerControl TheGoodzilla;
    private Vector3 LastGoodzillaPosition;
    private float DistanceToMove;


	// Use this for initialization
	void Start () {
        TheGoodzilla = FindObjectOfType<PlayerControl>();
        LastGoodzillaPosition = TheGoodzilla.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        DistanceToMove = TheGoodzilla.transform.position.x - LastGoodzillaPosition.x;
        transform.position = new Vector3(transform.position.x + DistanceToMove, transform.position.y, transform.position.z);

        LastGoodzillaPosition = TheGoodzilla.transform.position;
	
	}
}
