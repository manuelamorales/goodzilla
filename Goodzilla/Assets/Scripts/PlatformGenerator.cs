﻿using UnityEngine;
using System.Collections;

public class PlatformGenerator : MonoBehaviour {

    public GameObject ThePlatform;
    public Transform generationPoint;
    public float DistanceBetween;

    private float PlatformWidth;

    public float DistanceBetweenMin;
    public float DistanceBetweenMax;

    //public GameObject[] thePlatforms;
    private int platformSelector;
    private float[] platformWidths;

    public ObjectPooler[] theObjectPools;

    public float minHeight;
    public Transform maxHeightPoint;
    private float maxHeight;
    public float maxHeightChange;
    private float heightChange;

	// Use this for initialization
	void Start () {
        //PlatformWidth = ThePlatform.GetComponent<BoxCollider2D>().size.x;

        platformWidths = new float[theObjectPools.Length];

        for(int i=0;i < theObjectPools.Length; i++)
        {
            platformWidths[i] = theObjectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
        }

        minHeight = transform.position.y;
        maxHeight = maxHeightPoint.position.y;
	}

	
	// Update is called once per frame
	void Update () {
        if (transform.position.x < generationPoint.position.x) 
        {
            DistanceBetween = Random.Range(DistanceBetweenMin, DistanceBetweenMax);


            platformSelector = Random.Range(0, theObjectPools.Length);

            heightChange = transform.position.y + Random.Range(maxHeightChange, -maxHeightChange);

            if(heightChange > maxHeight)
            {
                heightChange = maxHeight;
            }
            else if (heightChange < minHeight)
            {
                heightChange = minHeight;
            }

            transform.position = new Vector3((transform.position.x + (platformWidths[platformSelector] / 2) + DistanceBetween),heightChange, transform.position.z);

      
            //Instantiate (/*ThePlatform*/ thePlatforms[platformSelector], transform.position, transform.rotation);

            GameObject newPlatform = theObjectPools[platformSelector].GetPooledObject();

            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);

            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2), transform.position.y, transform.position.z);
        }
	
	}
}
