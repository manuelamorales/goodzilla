﻿using UnityEngine;
using System.Collections;

public class PlatformGenerator : MonoBehaviour {
    public GameObject ThePlatform;
    public Transform generationPoint;
    public float DistanceBetween;
    private float PlatformWidth;
    public float DistanceBetweenMin;
    public float DistanceBetweenMax;

	// Use this for initialization
	void Start () {

	PlatformWidth = ThePlatform.GetComponent<BoxCollider2D>().size.x;

	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x < generationPoint.position.x) 
        {
            DistanceBetween = Random.Range(DistanceBetweenMin, DistanceBetweenMax);
         transform.position = new Vector3((transform.position.x + PlatformWidth + DistanceBetween), transform.position.y, transform.position.z);
            Instantiate (ThePlatform, transform.position, transform.rotation);
        }
	
	}
}
