﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
    public float moveSpeed;
    public float jumpForce;

    private Rigidbody2D myRigidbody;

    public bool grounded;
    public LayerMask whatIsGround;
    public Transform GroundCheck;
    public float GroundCheckRadius;

    // private Collider2D myCollider;
    private Animator myAnimator;

     

	// Use this for initialization
	void Start () {
        myRigidbody = GetComponent<Rigidbody2D>();
        // myCollider = GetComponent<Collider2D>();
        myAnimator = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {

        // grounded = Physics2D.IsTouchingLayers(myCollider, WhatIsGround);
        grounded = Physics2D.OverlapCircle(GroundCheck.position, GroundCheckRadius, whatIsGround);

        myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.y);
        
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButton(0))
        {
            if (grounded)
            { myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce); }
        }

        myAnimator.SetBool("Grounded", grounded);
	}
}
